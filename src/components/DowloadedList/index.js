import React, { useState, useEffect } from 'react'
import {
  Divider,
  Button,
  AnchorButton
} from '@blueprintjs/core'
import {
  ErrorToast
} from 'components'
import {
  getVideoStream,
  getVideoThumb,
  getVideoList,
  getVideoInfoYoutubeDl,
  removeVideo as deleteVideo
} from 'helpers/request-helper'
import { useCounterState } from 'context/DownloadCounterContext'
import PaginatedContainer from '../PaginatedContainer'
import withFilterAndSort from '../FilteredAndSortedContainer'
import './style.scss'

const sortFilterFactory = {
  filterTitle: (item, filterTerm) => item.title.search(new RegExp(`(${filterTerm.replace(/ /g, '.?')})`, 'gi')) > -1,
  sortOptions: [
  {
    name: 'Date',
    sortAsc: (b, a) => new Date(b.updatedAt) - new Date(a.updatedAt),
    sortDesc: (a, b) => new Date(b.updatedAt) - new Date(a.updatedAt)
  },
  {
    name: 'Size',
    sortAsc: (b, a) => b.size - a.size,
    sortDesc: (a, b) => b.size - a.size
  }
]}
const FilterContainer = withFilterAndSort(PaginatedContainer, sortFilterFactory)

export default function DowloadList(props) {
  const [openRows, setOpenRows] = useState([])
  const [videoList, setVideoList] = useState([])
  const { currentDownloads } = useCounterState()

  useEffect(() => {
    getVideoList()
      .then(setVideoList)
      .catch(ErrorToast)
  }, [currentDownloads.length])

  const { setVideoInfo, openVideo } = props

  const isOpen = (item) => {
    return openRows.indexOf(item) > -1
  }

  const toggleOpen = (item) => {
    const list = openRows.slice()
    const idx = list.indexOf(item)
    if (idx > -1) list.splice(idx, 1)
    else list.push(item)
    setOpenRows(list)
  }

  const removeVideo = (id, deleteFile = false) => {
    const updatedVideoList = videoList.filter(a => a.id !== id)
    deleteVideo({ id, deleteFile })
      .then(() => setVideoList(updatedVideoList))
      .catch(err => err)
  }

  const retryDownload = (id) => {
    const url = videoList.filter(info => info.id === id)[0].url
    getVideoInfoYoutubeDl(url)
      .then(setVideoInfo)
      .catch(err => err)
  }

  if (!videoList || videoList.length === 0) return null

  const sortedList = videoList.sort((a, b) => new Date(b.updatedAt) - new Date(a.updatedAt))

  return (
    <div className='download-list-container'>
      <div className='list-header'>Downloaded</div>
      <FilterContainer
        itemList={sortedList}
        itemsPerPage={10}
        renderComponent={(idx, info) =>
          (<DownloadListRow
            key={idx}
            rowId={idx}
            videoInfo={info}
            isOpen={isOpen}
            toggleOpen={toggleOpen}
            thumbnail={getVideoThumb(info.thumbnail)}
            actions={{ retryDownload, openVideo, removeVideo }}
          />)
        }
      />
    </div>
  )
}

const DownloadListRow = (props) => {
  const { rowId, videoInfo: { title, duration, size, id }, isOpen, toggleOpen, thumbnail, actions } = props

  return (
    <div className={`item-row-container ${isOpen(rowId) ? 'is-open' : ''}`}>
      <div className='item-container' onClick={() => toggleOpen(rowId)}>
        <img className='item-thumb' src={thumbnail} alt='thumb' />
        <span className='item-name'>{title}</span>
        <div className='size-container'>
          <span className='item-size'>{duration}</span>
          <span className='item-size'>{processSize(size)}</span>
        </div>
      </div>
      <DownloadListRowActions videoId={id} {...actions} />
      <Divider className='item-row-divider' />
    </div>
  )
}

const DownloadListRowActions = (props) => {
  const { videoId, openVideo, removeVideo, retryDownload } = props

  const videoDownloadUrl = (id) => getVideoStream(id) + '&download=true'

  return (
    <div className='item-actions-container'>
      <Button
        className='play-item'
        icon='play'
        minimal='true'
        onClick={() => openVideo(videoId)}
      />
      <AnchorButton
        className='download-item'
        icon='download'
        minimal='true'
        href={videoDownloadUrl(videoId)}
      />
      <Button
        className='remove-item'
        icon='remove'
        minimal='true'
        onClick={() => removeVideo(videoId)}
      />
      <Button
        className='delete-item'
        icon='delete'
        minimal='true'
        onClick={() => removeVideo(videoId, true)}
      />
      <Button
        className='retry-item'
        icon='refresh'
        minimal='true'
        onClick={() => retryDownload(videoId)}
      />
    </div>
  )
}

function processSize(filesize) {
  if (filesize / (1024 * 1024 * 1024) > 1) return `${(filesize / (1024 * 1024 * 1024)).toFixed(1)} GB`
  else if (filesize / (1024 * 1024) > 1) return `${(filesize / (1024 * 1024)).toFixed(1)} MB`
  return `${(filesize / 1024).toFixed(1)} KB`
}
