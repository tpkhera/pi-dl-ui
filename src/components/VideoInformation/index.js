import React from 'react'
import {
  Button,
  Card,
  H5,
  Icon,
  Text,
  Divider
} from '@blueprintjs/core'
import {
  ErrorToast
} from 'components'
import { downloadVideoYoutubeDl } from 'helpers/request-helper'
import { addDownload, useCounterDispatch } from 'context/DownloadCounterContext'
import './style.scss'

export default function VideoInformation (props) {
  const { videoInfo, onReset } = props
  const counterDispatch = useCounterDispatch()

  if (!videoInfo) return null

  const processedVideoInfo = processVideoInfo(Array.isArray(videoInfo) ? videoInfo[0] : videoInfo)

  const { thumbnail, title, duration, webpage_url, formats } = processedVideoInfo

  const selectFormat = (idx) => {
    const format = idx ? formats[idx - 1] : {}
    downloadVideo({
      thumbnail,
      title,
      duration,
      format,
      webpage_url
    })
  }

  const downloadVideo = (videoInfo) => {
    const { webpage_url: url, thumbnail, title, duration, format: { format_id } } = videoInfo

    downloadVideoYoutubeDl({ url, thumbnail, title, duration, format_id })
      .then(res => { addDownload(counterDispatch, { [res.id]: res }); onReset() })
      .catch(ErrorToast)
  }

  return (
    <div className='video-info-container'>
      <Card className='video-info-card'>
        <img className='video-format-thumbnail' alt='thumb' src={thumbnail} />
        <H5>{title}</H5>
        <Divider />
        <div className='video-format-thumbnail-info'>
          <Button
            text={duration}
            minimal='true'
            icon='time'
          />
          <Divider />
          <Button
            text='Best'
            minimal='true'
            icon='download'
            intent='primary'
            onClick={() => selectFormat()}
          />
        </div>
      </Card>
      <Text className='video-format-container-header'>Available Formats</Text>
      <Card className='video-format-container'>
        {formats.map((item, idx) => {
          return (
            <React.Fragment key={idx}>
              <div className='video-format-card' onClick={() => selectFormat(idx + 1)}>
                <Icon icon={item.height ? 'video' : 'music'} iconSize={32} color='#999' />
                <Text>{item.height ? `${item.height}p` : ''}</Text>
                <Text>{item.ext}</Text>
                <Text>{processSize(item.filesize)}</Text>
              </div>
              <Divider />
            </React.Fragment>
          )
        })}
      </Card>
      <Button
        text='Clear'
        minimal='true'
        intent='danger'
        onClick={onReset}
      />
    </div>
  )
}

function processVideoInfo (videoInfo) {
  const processedVideoInfo = {
    extractor: videoInfo.extractor,
    duration: videoInfo.duration || 0,
    thumbnail: videoInfo.thumbnails
      ? videoInfo.thumbnails[0].url
      : videoInfo.thumbnail || 'https://dyncdn.me/static/20/img/16x16/download.png',
    title: videoInfo.title,
    webpage_url: videoInfo.webpage_url,
    formats: processVideoFormats(videoInfo)
  }
  return processedVideoInfo
}

function processVideoFormats (videoInfo) {
  let videoFormats
  try {
    videoFormats = extractVideoFormats(videoInfo.formats)
  } catch (error) {
    videoFormats = [{
      url: videoInfo.url,
      ext: videoInfo.ext,
      format: videoInfo.format,
      filesize: videoInfo.filesize,
      height: videoInfo.height,
      format_id: videoInfo.format_id
    }]
  }
  return videoFormats
}

function extractVideoFormats (formats) {
  return formats.map(format => {
    return {
      url: format.url,
      ext: format.ext,
      format: format.format,
      filesize: format.filesize,
      height: format.height,
      format_id: format.format_id
    }
  }).sort((a, b) => {
    return b.height - a.height
  })
}

function processSize (filesize) {
  if (filesize / (1024 * 1024 * 1024) > 1) return `${(filesize / (1024 * 1024 * 1024)).toFixed(1)} GB`
  else if (filesize / (1024 * 1024) > 1) return `${(filesize / (1024 * 1024)).toFixed(1)} MB`
  return `${(filesize / 1024).toFixed(1)} KB`
}
