import React, { useState, useEffect } from 'react'
import { Button } from '@blueprintjs/core'
import PropTypes from 'prop-types'
import './style.scss'

const PaginatedContainer = (props) => {
  const { itemList, itemsPerPage, numberOfPageControls, renderComponent } = props
  const numberOfPages = Math.ceil(itemList.length / itemsPerPage)
  const initPageItems = itemList.slice(0, itemsPerPage)
  const initPageControls = Array(Math.min(numberOfPages, numberOfPageControls)).fill(1).map((x, i) => i + 1)

  const [currentPageNumber, setCurrentPageNumber] = useState(1)
  const [pageItems, setPageItems] = useState(initPageItems)
  const [pageControls, setPageControls] = useState(initPageControls)

  const changedItemList = itemList.length + itemList.map(x => x.title).join('')
  useEffect(() => {
    setPageItems(initPageItems)
    setPageControls(initPageControls)
  }, [changedItemList])
  
  const setPageNumber = (idx) => {
    setCurrentPageNumber(idx)
    setPageItems(itemList.slice((idx - 1) * itemsPerPage, (idx - 1) * itemsPerPage + itemsPerPage))
    if (idx > pageControls[Math.ceil(numberOfPageControls / 2)] && pageControls[pageControls.length - 1] < numberOfPages) {
      setPageControls(pageControls.map(x => x + 1))
    }
    if (idx < pageControls[Math.ceil(numberOfPageControls / 2)] && pageControls[0] > 1) {
      setPageControls(pageControls.map(x => x - 1))
    }
    window.scrollTo(0, 0)
  }

  return (
    <div className="paginated-container">
      <div className="page-container">
        {
          pageItems.map((item, idx) => renderComponent(idx, item))
        }
      </div>
      <div className="page-number-controls-container">
        <Button icon="chevron-left" disabled={currentPageNumber === 1} onClick={() => setPageNumber(currentPageNumber - 1)}></Button>
        {
          pageControls.map((x, i) => <Button key={i} text={x} onClick={() => setPageNumber(x)} active={currentPageNumber === x}></Button>)
        }
        <Button icon="chevron-right" disabled={currentPageNumber === numberOfPages} onClick={() => setPageNumber(currentPageNumber + 1)}></Button>
      </div>
    </div>
  )
}

PaginatedContainer.propTypes = {
  itemList: PropTypes.array.isRequired,
  renderComponent: PropTypes.func.isRequired,
  itemsPerPage: PropTypes.number.isRequired,
  numberOfPageControls: PropTypes.number.isRequired,
}

PaginatedContainer.defaultProps = {
  itemsPerPage: 20,
  numberOfPageControls: 5
}

export default PaginatedContainer
