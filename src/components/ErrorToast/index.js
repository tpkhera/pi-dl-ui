import { Toaster } from '@blueprintjs/core'

const ErrorToast = ({ message }) => {
  const Toast = Toaster.create({
    className: 'error-toast'
  })
  Toast.show({
    icon: 'warning-sign',
    intent: 'danger',
    message
  })
}

export default ErrorToast
