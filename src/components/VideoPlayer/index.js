import React from 'react'
import {
  Card,
  Button
} from '@blueprintjs/core'
import { getVideoStream } from 'helpers/request-helper'
import './style.scss'

export default function VideoPlayer (props) {
  const { videoId, onClose } = props

  if (!videoId && videoId !== 0) return null

  return (
    <div className='video-player-container'>
      <Card className='video-format-container'>
        <video controls autoPlay src={getVideoStream(videoId)} type='video/mp4' />
        <Button text='Close' onClick={onClose} intent='danger' minimal='true' />
      </Card>
    </div>
  )
}
