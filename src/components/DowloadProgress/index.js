import React, { useState, useEffect } from 'react'
import io from 'socket.io-client'
import {
  ProgressBar,
  Text
} from '@blueprintjs/core'
import {
  ErrorToast
} from 'components'
import {
  getSocketIoUrl,
  getCurrentDownloadsList
} from 'helpers/request-helper'
import {
  useCounterDispatch,
  useCounterState,
  removeDownload,
  addDownload
} from 'context/DownloadCounterContext'
import './style.scss'

export default function DowloadProgress (props) {
  var { currentDownloads } = useCounterState()
  const counterDispatch = useCounterDispatch()

  useEffect(() => {
    getCurrentDownloadsList()
      .then(res => addDownload(counterDispatch, res))
      .catch(ErrorToast)
    // eslint-disable-next-line
  }, [])

  const onDownloadComplete = (item) => {
    removeDownload(counterDispatch, item)
  }

  const getSocket = (ns) => {
    return io(`${getSocketIoUrl}/${ns}`, {
      transports: ['websocket'],
      upgrade: false
    })
  }

  if (currentDownloads.length === 0) return null

  return (
    <div className='download-progress-container'>
      <div className='list-header'>Downloading</div>
      {
        currentDownloads.map((a, idx) => {
          return (
            <DowloadProgressRow
              key={idx}
              videoInfo={Object.values(a)[0]}
              socket={getSocket(Object.keys(a)[0])}
              onDownloadComplete={onDownloadComplete}
            />
          )
        })
      }
    </div>
  )
}

function DowloadProgressRow (props) {
  const [progress, setProgress] = useState(0)
  const { socket, onDownloadComplete, videoInfo } = props

  useEffect(() => {
    socket.on('progress', (data) => {
      const { status } = data
      if (status === 100) {
        onDownloadComplete({ [videoInfo.id]: videoInfo })
        return
      }
      setProgress(status)
    })
    // eslint-disable-next-line
  }, [])

  if (!videoInfo) return null

  const { thumbnail, title, size } = videoInfo

  if (!progress) return null

  return (
    <div className='downloading-container'>
      <img className='item-thumb' src={thumbnail} alt='thumb' />
      <span className='item-name'>{title}</span>
      <div className='progress-container'>
        <ProgressBar value={progress / 100} intent='primary' />
        <Text>{processSize(size)}</Text>
      </div>
    </div>
  )
}

function processSize (filesize) {
  if (filesize / (1024 * 1024 * 1024) > 1) return `${(filesize / (1024 * 1024 * 1024)).toFixed(1)} GB`
  else if (filesize / (1024 * 1024) > 1) return `${(filesize / (1024 * 1024)).toFixed(1)} MB`
  return `${(filesize / 1024).toFixed(1)} KB`
}
