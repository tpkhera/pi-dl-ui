import React, { useState } from 'react'
import {
  Button,
  InputGroup
} from '@blueprintjs/core'
import {
  ErrorToast
} from 'components'
import { getVideoInfoYoutubeDl } from 'helpers/request-helper'
import './style.scss'

export default function SubmitVideoUrl (props) {
  const [url, setUrl] = useState('')
  const [isLoading, setLoading] = useState(false)

  const getVideoInfo = (ev) => {
    ev.preventDefault()

    const { setVideoInfo } = props

    setLoading(true)
    getVideoInfoYoutubeDl(url)
      .then(res => {
        setUrl('')
        setLoading(false)
        setVideoInfo(res)
      })
      .catch(err => {
        setLoading(false)
        ErrorToast(err)
      })
  }

  const pasteUrl = () => {
    // navigator.permissions.query({
    //   name: 'clipboard-read'
    // }).then(permissionStatus => {
    //   // Will be 'granted', 'denied' or 'prompt':
    //   alert(permissionStatus.state);

    //   // Listen for changes to the permission state
    //   permissionStatus.onchange = () => {
    //     alert(permissionStatus.state);
    //   };
    // });
    navigator.clipboard.readText().then((text) => {
      this.setState({ url: text })
    })
  }

  return (
    <div className='video-url-submit-form'>
      <form>
        <div className='input-container'>
          <Button
            icon='clipboard'
            minimal='true'
            onClick={pasteUrl}
          />
          <InputGroup
            className='url-input'
            name='url'
            onChange={(ev) => setUrl(ev.target.value)}
            type='text'
            value={url}
          />
        </div>

        <Button
          alignText='center'
          intent='primary'
          large='true'
          fill='true'
          loading={isLoading}
          onClick={getVideoInfo}
          text='Get Info'
          type='submit'
        />
      </form>
    </div>
  )
}
