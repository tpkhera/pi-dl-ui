import React, { useState } from 'react'
import { InputGroup, HTMLSelect, Button } from '@blueprintjs/core'
import './style.scss'

function withFilterAndSort (Component, sortFilterFactory) {
  const { filterTitle, sortOptions } = sortFilterFactory
  const pageSortOptions = sortOptions.map(x => x.name)

  return function FilteredAndSortedContainer(props) {
    const { itemList, ...rest } = props

    const [filterTerm, setFilterTerm] = useState('')
    const [filteredList, setFilteredList] = useState(itemList)
    const [sortOrder, setSortOrder] = useState(true)
    const [sortBy, setSortBy] = useState(pageSortOptions[0])

    const updateFilterTerm = (term) => {
      setFilterTerm(term)
      setFilteredList(itemList.filter(item => filterTitle(item, term)))
    }

    const updateSortBy = (value) => {
      setSortBy(value)
      updateFilteredList(value, sortOrder)
    }

    const toggleSort = () => {
      const newSortOrder = !sortOrder
      setSortOrder(newSortOrder)
      updateFilteredList(sortBy, newSortOrder)
    }

    const updateFilteredList = (sortBy, sortOrder) => {
      const tempOrder = sortOptions.filter(x => x.name === sortBy)[0]
      setFilteredList(filteredList.sort(sortOrder ? tempOrder.sortAsc : tempOrder.sortDesc))
    }
    
    return (
      <div className="filtered-container">
        <div className="filter-container">
          <InputGroup
            className='filter-input'
            name='filter'
            onChange={(ev) => updateFilterTerm(ev.target.value)}
            type='text'
            leftIcon='search'
            placeholder='Filter'
            value={filterTerm}
          />
          <HTMLSelect 
            onChange={(ev) => updateSortBy(ev.currentTarget.value)} 
            value={sortBy}
            options={pageSortOptions}
          />
          <Button 
            onClick={toggleSort}
            icon={sortOrder ? 'sort-asc' : 'sort-desc'}
          />
        </div>
        <Component itemList={filteredList} {...rest} />
      </div>
    )
  }
}

export default withFilterAndSort
