import React from 'react'

import './style.scss'

export default function AppLogo (props) {
  return (
    <div className='app-logo'>
      <span>Accio</span>
    </div>
  )
}
