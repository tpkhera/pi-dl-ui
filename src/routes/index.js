import React from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom'

import CONSTANTS from 'helpers/constants-helper'
import { UserProvider, useUserState } from 'context/UserContext'
import App from './App'
import Login from './Login'

export default function AppRoutes () {
  const PrivateRoute = ({ component: Component, ...rest }) => {
    const userState = useUserState()
    return (
      <Route
        {...rest} render={(props) => (
          userState.isAuthenticated
            ? <Component {...props} />
            : <Redirect to={{ pathname: CONSTANTS.ROUTE_URL_LOGIN, state: { from: props.location } }} />
        )}
      />
    )
  }

  return (
    <UserProvider>
      <Router>
        <Switch>
          <PrivateRoute exact path={CONSTANTS.ROUTE_URL_APP} component={App} />
          <Route exact path={CONSTANTS.ROUTE_URL_LOGIN} component={Login} />
        </Switch>
      </Router>
    </UserProvider>
  )
}
