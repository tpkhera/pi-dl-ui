import React, { useState } from 'react'
import {
  Button,
  InputGroup
} from '@blueprintjs/core'
import {
  AppLogo,
  ErrorToast
} from 'components'

import { loginUser, useUserDispatch } from 'context/UserContext'
import { initiateLogin } from 'helpers/request-helper'
import './style.scss'

function Login (props) {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [isLoading, setLoading] = useState(false)
  const userDispatch = useUserDispatch()
  const { history } = props

  const initLogin = (ev) => {
    ev.preventDefault()

    setLoading(true)
    initiateLogin({ username, password })
      .then(res => {
        setLoading(false)
        setUsername('')
        setPassword('')
        loginUser(userDispatch, history)
      })
      .catch(err => {
        ErrorToast(err)
        setLoading(false)
      })
  }

  return (
    <div className='login-form-container bp3-dark'>
      <div className='login-form bp3-card'>
        <AppLogo />
        <form>
          <div className='input-container'>
            <InputGroup
              className='username-input'
              name='username'
              placeholder='Username'
              onChange={(ev) => setUsername(ev.target.value)}
              type='text'
              value={username}
            />
            <InputGroup
              className='password-input'
              name='password'
              placeholder='Password'
              onChange={(ev) => setPassword(ev.target.value)}
              type='password'
              value={password}
            />
          </div>

          <Button
            alignText='center'
            intent='primary'
            large='true'
            fill='true'
            loading={isLoading}
            onClick={initLogin}
            text='Login'
            type='submit'
          />
        </form>
      </div>
    </div>
  )
}

export default Login
