import React, { Component } from 'react'
import {
  AppLogo,
  SubmitVideoUrl,
  VideoInformation,
  VideoPlayer,
  DowloadProgress,
  DowloadedList
} from 'components'
import { CounterProvider } from 'context/DownloadCounterContext'
import './style.scss'

export default class App extends Component {
  state = {
    videoInfo: null,
    videoId: null
  }

  setVideoInfo = (videoInfo) => {
    this.setState({ videoInfo })
  }

  openVideo = (videoId) => {
    this.setState({ videoId })
  }

  render() {
    return (
      <div className='app-container bp3-dark'>
        <div className='app-form-container bp3-card'>
          <CounterProvider>
            <AppLogo />
            <SubmitVideoUrl
              setVideoInfo={this.setVideoInfo}
            />
            <VideoInformation
              videoInfo={this.state.videoInfo}
              onReset={() => this.setVideoInfo(null)}
            />
            <VideoPlayer
              videoId={this.state.videoId}
              onClose={() => this.openVideo(null)}
            />
            <DowloadProgress />
            <DowloadedList
              openVideo={this.openVideo}
              setVideoInfo={this.setVideoInfo}
            />
          </CounterProvider>
        </div>
      </div>
    )
  }
}
