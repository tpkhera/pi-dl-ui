import React from 'react'

var CounterStateContext = React.createContext()
var CounterDispatchContext = React.createContext()

function counterReducer (state, action) {
  const { currentDownloads } = state

  switch (action.type) {
    case 'ADD_DOWNLOAD':
      Array.isArray(action.item)
        ? currentDownloads.push(...action.item)
        : currentDownloads.push(action.item)
      return { ...state, currentDownloads }
    case 'REMOVE_DOWNLOAD':
      currentDownloads.splice(currentDownloads.indexOf(action.item), 1)
      return { ...state, currentDownloads }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`)
    }
  }
}

function CounterProvider ({ children }) {
  var [state, dispatch] = React.useReducer(counterReducer, {
    currentDownloads: []
  })
  return (
    <CounterStateContext.Provider value={state}>
      <CounterDispatchContext.Provider value={dispatch}>
        {children}
      </CounterDispatchContext.Provider>
    </CounterStateContext.Provider>
  )
}

function useCounterState () {
  var context = React.useContext(CounterStateContext)
  if (context === undefined) {
    throw new Error('useCounterState must be used within a CounterProvider')
  }
  return context
}

function useCounterDispatch () {
  var context = React.useContext(CounterDispatchContext)
  if (context === undefined) {
    throw new Error('useCounterDispatch must be used within a CounterProvider')
  }
  return context
}

export { CounterProvider, useCounterState, useCounterDispatch, addDownload, removeDownload }

// ###########################################################
function addDownload (dispatch, item) {
  dispatch({
    type: 'ADD_DOWNLOAD',
    item
  })
}

function removeDownload (dispatch, item) {
  dispatch({
    type: 'REMOVE_DOWNLOAD',
    item
  })
}
