import React from 'react'

import CONSTANTS from 'helpers/constants-helper'

var UserStateContext = React.createContext()
var UserDispatchContext = React.createContext()

function userReducer (state, action) {
  switch (action.type) {
    case 'LOGIN_SUCCESS':
      return { ...state, isAuthenticated: true }
    case 'SIGN_OUT_SUCCESS':
      return { ...state, isAuthenticated: false }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`)
    }
  }
}

function UserProvider ({ children }) {
  var [state, dispatch] = React.useReducer(userReducer, {
    isAuthenticated: document.cookie.indexOf(CONSTANTS.AUTH_COOKIE_KEY) > -1
  })

  return (
    <UserStateContext.Provider value={state}>
      <UserDispatchContext.Provider value={dispatch}>
        {children}
      </UserDispatchContext.Provider>
    </UserStateContext.Provider>
  )
}

function useUserState () {
  var context = React.useContext(UserStateContext)
  if (context === undefined) {
    throw new Error('useUserState must be used within a UserProvider')
  }
  return context
}

function useUserDispatch () {
  var context = React.useContext(UserDispatchContext)
  if (context === undefined) {
    throw new Error('useUserDispatch must be used within a UserProvider')
  }
  return context
}

export { UserProvider, useUserState, useUserDispatch, loginUser, signOut }

// ###########################################################

function loginUser (dispatch, history) {
  dispatch({ type: 'LOGIN_SUCCESS' })
  history.push(CONSTANTS.ROUTE_URL_APP)
}

function signOut (dispatch, history) {
  dispatch({ type: 'SIGN_OUT_SUCCESS' })
  history.push(CONSTANTS.ROUTE_URL_LOGIN)
}
