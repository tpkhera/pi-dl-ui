import React from 'react'
import ReactDOM from 'react-dom'
import './index.scss'
import AppRoutes from './routes'
import * as serviceWorker from './serviceWorker'

const render = Component => {
  return ReactDOM.render(
    <Component />,
    document.getElementById('root')
  )
}

render(AppRoutes)

if (module.hot) {
  module.hot.accept('./routes', () => {
    const NextApp = require('./routes').default
    render(NextApp)
  })
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
