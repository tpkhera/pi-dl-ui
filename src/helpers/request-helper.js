import CONSTANTS from './constants-helper'

const postData = async (url, data) => {
  const res = await fetch(url, {
    method: 'POST',
    mode: 'cors',
    credentials: 'include',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  })
  if (res.status !== 200) handleError(res)
  const resJson = await res.json()
  return resJson
}

const getData = async (url) => {
  const res = await fetch(url, {
    method: 'GET',
    mode: 'cors',
    credentials: 'include'
  })
  if (res.status !== 200) handleError(res)
  const resJson = await res.json()
  return resJson
}

const handleError = ({ status, statusText }) => {
  switch (status) {
    case 401:
      throw new Error('Auth error')
    default:
      throw new Error(statusText)
  }
}

const initiateLogin = (requestObject) => {
  return postData(CONSTANTS.API_URL_LOGIN_USER, requestObject)
}

const getVideoList = () => {
  return getData(CONSTANTS.API_URL_GET_VIDEO_LIST)
}

const getCurrentDownloadsList = () => {
  return getData(CONSTANTS.API_URL_GET_CURRENT_DOWNLOADS_LIST)
}

const getVideoInfoYoutubeDl = (url) => {
  return postData(CONSTANTS.API_URL_GET_VIDEO_INFO_YOUTUBEDL, { url })
}

const downloadVideoYoutubeDl = (requestObject) => {
  return postData(CONSTANTS.API_URL_DOWNLOAD_VIDEO_YOUTUBEDL, requestObject)
}

const removeVideo = (requestObject) => {
  return postData(CONSTANTS.API_URL_REMOVE_VIDEO, requestObject)
}

const getVideoStream = CONSTANTS.API_URL_GET_VIDEO_STREAM

const getSocketIoUrl = CONSTANTS.SOCKET_IO_URL

const getVideoThumb = CONSTANTS.API_URL_GET_THUMBNAIL

export {
  initiateLogin,
  getVideoList,
  getCurrentDownloadsList,
  getVideoInfoYoutubeDl,
  downloadVideoYoutubeDl,
  removeVideo,
  getVideoStream,
  getSocketIoUrl,
  getVideoThumb
}
