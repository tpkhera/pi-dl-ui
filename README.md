# Accio

Accio is a self-hosted web application which enables you to download and serve videos from 
a multitude of video-hosting websites. It utilizes the excellent youtube-dl under the hood
to extract and fetch the videos.

<img src="assets/1.png" width="200" />
<img src="assets/2.png" width="200" />
<img src="assets/3.png" width="200" />
<img src="assets/4.png" width="200" />

### Prerequisites

- [yarn](https://classic.yarnpkg.com/en/docs/install) / [npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)
- [accio-server](https://gitlab.com/tpkhera/pi-dl)

### Getting started

```bash
git clone https://gitlab.com/tpkhera/pi-dl-ui accio-ui
cd accio-ui
```
In the `accio-ui` directory, create a new `.env` file with the folowing variables. These should correspond to the values set in [accio-server](https://gitlab.com/tpkhera/pi-dl)
```bash
REACT_APP_API_SERVER_HOST=http://localhost
REACT_APP_API_SERVER_PORT=3001
REACT_APP_SOCKET_IO_HOST=http://localhost
REACT_APP_SOCKET_IO_PORT=3002
```
After setting up `.env` and starting [accio-server](https://gitlab.com/tpkhera/pi-dl), run
```bash
yarn
yarn start
```
