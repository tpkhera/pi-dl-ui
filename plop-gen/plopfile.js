const componentGenerator = require('./generators/components')
const routeGenerator = require('./generators/routes')

module.exports = function (plop) {
  plop.setGenerator('Component', componentGenerator)
  plop.setGenerator('Route', routeGenerator)
}
