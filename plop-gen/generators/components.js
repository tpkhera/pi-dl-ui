const { componentGeneratorPrompts, componentGeneratorActions } = require('./helpers/utils')

const componentGenerator = {
  description: 'Create a Component',
  prompts: componentGeneratorPrompts,
  actions: componentGeneratorActions
}

module.exports = componentGenerator
