const { routeGeneratorPrompts, routeGeneratorActions } = require('./helpers/utils')

const routeGenerator = {
  description: 'Scaffold a route',
  prompts: routeGeneratorPrompts,
  actions: routeGeneratorActions
}

module.exports = routeGenerator
