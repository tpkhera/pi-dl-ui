const createIndex = (folder) => {
  return {
    type: 'add',
    path: `../src/${folder}/{{pascalCase name}}/index.js`,
    templateFile: `./templates/${folder}.hbs`
  }
}

const createStyle = (folder) => {
  return {
    type: 'add',
    path: `../src/${folder}/{{pascalCase name}}/style.scss`
  }
}

const getTemplateName = (type) => {
  return {
    type: 'input',
    name: 'name',
    message: `What is the ${type} name?`,
    validate: function (value) {
      if ((/.+/).test(value)) {
        return true
      }
      return 'name is required'
    }
  }
}

const componentGeneratorPrompts = [getTemplateName('component')]
const componentGeneratorActions = [createIndex('components'), createStyle('components')]

const routeGeneratorPrompts = [getTemplateName('route')]
const routeGeneratorActions = [createIndex('routes'), createStyle('routes')]

module.exports = {
  componentGeneratorPrompts,
  componentGeneratorActions,
  routeGeneratorPrompts,
  routeGeneratorActions
}
